package services;

import models.networking.Client;
import models.networking.Server;

/**
 * Created by Aivaras on 14/05/2016.
 */
public class MultiplayerServices {
    private static Server server;
    private static Client client;
    private static Thread communication;
    private static boolean serverConnection = false;

    /**
     * set connection as a client
     */
    public static void setClientConnection() {
        client = new Client("127.0.0.1", 9999);
        communication = new Thread(client);
        communication.start();
    }

    /**
     * set connection as a server
     */
    public static void setServerConnection() {
        server = new Server();
        communication = new Thread(server);
        communication.start();
        serverConnection = true;
    }

    /**
     * close server connection
     */
    public static void closeServerConnection() {
        server.terminate();
        try {
            communication.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        serverConnection = false;
    }

    /**
     * close client connection
     */
    public static void closeClientConnection() {
        client.terminate();
        try {
            communication.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * send data to remote server/client
     *
     * @param command
     */
    public static void sendData(String command) {
        if (serverConnection) {
            server.sendData(command);
        }
    }

    /**
     * communicate with game to move opponent
     *
     * @param data - opponent actions
     */
    public static void decode(String data) {
        //TODO add game functionality calls
    }
}
