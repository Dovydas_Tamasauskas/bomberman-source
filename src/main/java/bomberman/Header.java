package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Header extends GameObject {

    public GameObject playerObject;

    public Handler handler;

    int time = 0, number = 1000;

    int i = 2, clock = 0;

    boolean endGame = false;

    public Header(ID id, Handler handler) {
        super(id);
        this.handler = handler;

    }

    public Rectangle getBounds() {
        return new Rectangle(0, 0, 0, 0);
    }

    public void tick() {
        if (time == 10) {
            time = 0;
            number--;

            if (number / (int) Math.pow(10, i) == 0 && i != 0) {
                i--;
            }
        }
        if (getWonTheGame() == false && getRunOutOfTime() == false) {
            clock++;
            time++;
        }
        if (clock > 29) {
            clock = 0;
        }
    }

    public void render(Graphics g) {
        drawHeaderImage(g);
        int a = i;

        for (int j = (int) Math.pow(10, i); j > 0; j /= 10) {
            drawNumberImage(g, number / j - number / (j * 10) * 10, 380 - a * 15, 17);
            a--;
        }

        drawClockImage(g, clock, 217, 27);

        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            int liv = tempObject.getLives();
            if (endGame) {
                tempObject.setWonTheGame(true);
            }
            if (number == 0) {
                tempObject.setRunOutOfTime(true);
            }
            if (liv == 0) {
                endGame = true;
            }
            if (tempObject.getId() == ID.Player) {
                drawNumberImage(g, liv, 35, 17);
            }
            if (tempObject.getId() == ID.Player2) {
                drawNumberImage(g, liv, 105, 17);
            }
        }
    }
}