package bomberman;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

    private int keyEvent[][] = {{KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_RIGHT, KeyEvent.VK_LEFT, KeyEvent.VK_SPACE}, {KeyEvent.VK_W, KeyEvent.VK_S, KeyEvent.VK_D, KeyEvent.VK_A, KeyEvent.VK_R}, {}};

    private Handler handler;

    public KeyInput(Handler handler) {
        this.handler = handler;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject.id == ID.Player) {
                keyPressedMethods(tempObject, key, 0);
            }
            if (tempObject.id == ID.Player2) {
                keyPressedMethods(tempObject, key, 1);
            }

        }
    }

    public void keyPressedMethods(GameObject tempObject, int key, int player) {
        boolean put = true;

        if (player(tempObject, key, player)) {

            GameObject tempObject2 = null;

            for (int i = 0; i < handler.getObject().size(); i++) {
                tempObject2 = handler.getObject().get(i);

                if (tempObject2.id == ID.Bomb) {
                    if (tempObject.getBounds().intersects(tempObject2.getBounds())) {
                        put = false;
                    }
                }
            }

            if (put == true && tempObject.getRunOutOfTime() == false &&
            tempObject.getWonTheGame() == false) {

                if (tempObject.GetMaxBombOnGame() > tempObject.GetCurrentlyBombOnGame()) {
                    int xx = tempObject.GetX(), yy = tempObject.GetY();

                    if (xx % 30 == 0) {
                        if (yy % 30 > 15) {
                            yy += 30;
                        }
                    } else if (yy % 30 == 0) {
                        if (xx % 30 > 15) {
                            xx += 30;
                        }
                    }

                    xx = xx / 30 * 30;
                    yy = yy / 30 * 30;

                    handler.addObject(new Bomb(xx, yy, ID.Bomb, handler, tempObject));
                    tempObject.AddBomb();
                }
            } else if (tempObject.getThrowBomb() && tempObject2 != null) {
                tempObject2.setThrowBomb(true);
            }

        }
    }

    public boolean player(GameObject tempObject, int key, int player) {
        if (key == keyEvent[player][0]) {
            tempObject.SetVely(-1 * tempObject.GetSpeed());
        }
        if (key == keyEvent[player][1]) {
            tempObject.SetVely(1 * tempObject.GetSpeed());
        }
        if (key == keyEvent[player][2]) {
            tempObject.SetVelx(1 * tempObject.GetSpeed());
        }
        if (key == keyEvent[player][3]) {
            tempObject.SetVelx(-1 * tempObject.GetSpeed());
        }
        if (key == keyEvent[player][4]) {
            return true;
        }

        return false;
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject.id == ID.Player) {
                keyReleasedMethods(tempObject, key, 0);
            }
            if (tempObject.id == ID.Player2) {
                keyReleasedMethods(tempObject, key, 1);
            }
        }
    }

    public void keyReleasedMethods(GameObject tempObject, int key, int player) {

        if (key == keyEvent[player][0]) {
            tempObject.SetVely(0);
        }
        if (key == keyEvent[player][1]) {
            tempObject.SetVely(0);
        }
        if (key == keyEvent[player][2]) {
            tempObject.SetVelx(0);
        }
        if (key == keyEvent[player][3]) {
            tempObject.SetVelx(0);
        }
    }
}
