package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Bomb extends GameObject {

    private static final int TIME_PASSED = 150;

    private Handler handler;

    private GameObject playerObject;

    public Bomb(int x, int y, ID id, Handler handler, GameObject playerObject) {
        super(x, y, id);
        this.handler = handler;
        this.playerObject = playerObject;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 30, 30);
    }

    private Rectangle getBounds(int xx, int yy) {
        return new Rectangle(xx, yy, 30, 30);
    }

    public void tick() {
        if (getTime() == TIME_PASSED || getExplodeByOtherBomb()) {
            x = explodeInChunk(x);
            y = explodeInChunk(y);
            renderBombExplotion();
            playerObject.RemoveBomb();
            handler.remuveObject(this);
        }

        if (!getThrowBomb()) {
            setDirection(playerObject.getDirection());
        }

        if (getThrowBomb()) {
            if (isCollitingWhenThrow()) {
                setVelWhenThrowing();
            } else {
                setThrowBomb(false);
                velx = 0;
                vely = 0;
            }
        }

        x += velx;
        y += vely;
        x = isOutOfMap(x);
        y = isOutOfMap(y - Game.w) + Game.w;

        if (!getThrowBomb()) {
            collision();
            addTime();
        }
    }

    public void render(Graphics g) {
        setImageTime(getImageTime()+1);
        if (getImageTime() % 100 == 0) {
            setImageTime2(getImageTime2()+1);
        }
        if (getImageTime2() == 4) {
            setImageTime2(0);
            setImageTime(0);
        }
        drawBombImage(g, getImageTime2(), 0);
    }

    private void setVelWhenThrowing() {
        if (getDirection() == 0) {
            velx = getBombSpeed();
            vely = 0;
        }
        if (getDirection() == 1) {
            velx = 0;
            vely = getBombSpeed();
        }
        if (getDirection() == 2) {
            velx = -getBombSpeed();
            vely = 0;
        }
        if (getDirection() == 3) {
            velx = 0;
            vely = -getBombSpeed();
        }
    }

    private int isOutOfMap(int i) {
        if (i == -30) {
            i = 450;
        } else if (i == 450) {
            i = -30;
        }

        return i;
    }

    private int explodeInChunk(int a) {
        if (a % 30 > 15) {
            a += 30;
        }

        return a / 30 * 30;
    }

    public void renderBombExplotion() {
        boolean[] dar = new boolean[4];
        boolean isLast = false;
        for (int i = 0; i < dar.length; i++) {
            dar[i] = true;
        }

        handler.addObject(new BombExplode(x, y, ID.BombExplode, handler, 6));

        for (int i = 1; i <= playerObject.GetBombDemage(); i++) {
            if (playerObject.GetBombDemage() == i) {
                isLast = true;
            }
            if (dar[0]) {
                dar[0] = isSpotEmpty(x + i * 30, y, 0, isLast);
            }
            if (dar[1]) {
                dar[1] = isSpotEmpty(x, y + i * 30, 1, isLast);
            }
            if (dar[2]) {
                dar[2] = isSpotEmpty(x - i * 30, y, 2, isLast);
            }
            if (dar[3]) {
                dar[3] = isSpotEmpty(x, y - i * 30, 3, isLast);
            }
        }
    }

    public boolean isSpotEmpty(int xx, int yy, int direct, boolean last) {
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            if (tempObject.getId() == ID.Bomb && tempObject != Bomb.this) {
                if (getBounds(xx, yy).intersects(tempObject.getBounds())) {
                    tempObject.setExplodeByOtherBomb(true);
                    return false;
                }
            }
            if (tempObject.getId() == ID.Border || tempObject.getId() == ID.Map ||
            (tempObject.getId() == ID.BombExplode && getExplodeByOtherBomb())) {
                if (getBounds(xx, yy).intersects(tempObject.getBounds())) {
                    return false;
                }
            }

            if (tempObject.getId() == ID.Block) {
                if (getBounds(xx, yy).intersects(tempObject.getBounds())) {
                    handler.addObject(new BombExplode(xx, yy, ID.BombExplode, handler, direct));
                    return false;
                }
            }

        }
        if (!last) {
            direct = direct % 2 + 4;
        }
        handler.addObject(new BombExplode(xx, yy, ID.BombExplode, handler, direct));
        return true;
    }

    private boolean isCollitingWhenThrow() {
        if (x % 30 != 0 || y % 30 != 0) {
            return true;
        }

        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            // TODO Simplify
            if ((tempObject.getId() == ID.Border || tempObject.getId() == ID.Block || tempObject.getId() == playerObject.id ||
            tempObject.getId() == ID.Map || tempObject.getId() == ID.Bomb || tempObject.getId() == ID.Enemy) && tempObject != Bomb.this) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    return true;
                }
            }
        }

        return false;
    }

    private void collision() {
        for (int i = 0; i < handler.getObject().size(); i++) {
            GameObject tempObject = handler.getObject().get(i);

            // Spyris
            if (tempObject.getId() == playerObject.id && getIsPlayerOnABomb()) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    int speed = playerObject.getBombSpeed();
                    int h[][] = {{speed, 0}, {0, speed}, {-speed, 0}, {0, -speed}};
                    int a = playerObject.getDirection();
                    for (int ii = 0; ii < h.length; ii++) {
                        velx = h[a][0];
                    }
                    vely = h[a][1];

                }
            }
            // ar zaidejas nulipes nuo bombos?
            if (tempObject.getId() == playerObject.id) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    setIsPlayerOnABomb(false);
                } else {
                    setIsPlayerOnABomb(true);
                }
            }

            if (tempObject.getId() == ID.Border || tempObject.getId() == ID.Block ||
            tempObject.getId() == ID.Map) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    x -= velx;
                    y -= vely;
                    velx = 0;
                    vely = 0;
                }
            }
            // Jei bombos tarpusavi lieciasi
            if (tempObject.getId() == ID.Bomb && tempObject != Bomb.this) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    x -= velx;
                    y -= vely;
                    velx = 0;
                    vely = 0;
                }
            }
        }
    }
}