package bomberman;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Player extends GameObject {
        
       private Handler handler;
       int playerSize = 30;
       
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
                this.handler = handler;
	}
        
        
        public Rectangle getBounds(){
            return new Rectangle(x, y,playerSize,playerSize);
        }
        
        public Rectangle getBounds(int x, int y){
            return new Rectangle(x, y,playerSize,playerSize);
        }
        
	public void tick() {
            
            if(getDead() == false && getWonTheGame() == false && getRunOutOfTime() == false){
                x += velx;
                y += vely;
            }
                collision();
                Direction();
                
                if(getMortality() == false){
                    addTime();
                    if(getTime() > 150) 
                        setMortality(true);
                }
                
                
	}

	public void render(Graphics g) {
            if(getDead())
                drawPlayerImage(g, getDeadAnimation(handler),4);
            else if(getWonTheGame())
                drawPlayerImage(g, getAnimation(150),5);
            else if(getRunOutOfTime())
                drawPlayerImage(g, getAnimation(150),6);
            else{
                if(getMortality() == true || getTime()%2 == 0)
		drawPlayerImage(g, getAnimation(150), getDirection());
            }
	}
        
    public void Direction(){
       if(velx > 0) setDirection(0);
       else if(velx < 0)
           setDirection(2);
       if(vely > 0) setDirection(1);
       else if(vely < 0)
           setDirection(3);
    }
    // Kai zaidejas slysta nesunaikinamom detalem
    // ir pritaikoma ar zaidejas tilps i kita kele
    public int helpPlayerWithOpsticals(int a1, int a2, int speed){
        int aa = lookForHoles(a1, speed);
        if(a1 != aa)
            a1 = aa;
        else
                    if((a2+30) % 60 == 0 && a1%30 !=0)
                        if(a1/30%2 == 0)
                            a1 += speed;
                        else 
                            a1 += -speed;

        return a1;
    }
    
    public int lookForHoles(int a, int speed){
        if(a%30+speed > 30)
            a+=30-a%30-speed;
        else if(a%30-speed < 0)
            a-=a%30;
        
        return a;
    }

    private void collision() {
        
        for(int i = 0; i < handler.getObject().size(); i++){
            GameObject tempObject = handler.getObject().get(i);
            
            if(tempObject.getId() == ID.Map)
                if(getBounds().intersects(tempObject.getBounds())){
                   x-=velx;
                   y-=vely;            
                   
                   x=helpPlayerWithOpsticals(x, y, GetSpeed());
                   y=helpPlayerWithOpsticals(y, x, GetSpeed());
                }
            if(tempObject.getId() == ID.Block || tempObject.getId() == ID.Border)
                if(getBounds().intersects(tempObject.getBounds())){
                   x-=velx;
                   y-=vely;
                   x=lookForHoles(x, GetSpeed());
                   y=lookForHoles(y, GetSpeed());
                }
            
            if((tempObject.getId() == ID.Enemy && tempObject.getDead() == false) || tempObject.getId() == ID.BombExplode)
                if(getBounds().intersects(tempObject.getBounds())){
                    if(getLives() != 0 && getWonTheGame() == false && getRunOutOfTime() == false)
                        if(getMortality()){
                            setLives(-1);
                            if(getLives() == 0){
                                setDead(true);
                                nullImagetime();
                            }else{
                                setMortality(false);
                                setTime(0);
                            }
                        }
                }
            
            if(tempObject.getId() == ID.Bomb)
                if(getBounds().intersects(tempObject.getBounds()) && tempObject.getIsPlayerOnABomb()){
                    //Jei nepaemes zaidejas spyrio - bombos nespiria
                    if(GetKickBomb() == false){
                       x-=velx;
                       y-=vely;
                    }
                    //Neleidzia zaidejui istrigti i kito zaidejo padeta ant jo bomba
                    if(getBounds(x-velx, y-vely).intersects(tempObject.getBounds())){
                       x+=velx;
                       y+=vely;
                    }
                }
           
            //---------------- GIFTS
            
            if(tempObject.getId() == ID.GiftBomb)
                if(getBounds().intersects(tempObject.getBounds())){
                    addMaxBombOnGame();
                 //  System.out.println("max bomb "+GetMaxBombOnGame());
                }
            if(tempObject.getId() == ID.GiftBombDemage)
                if(getBounds().intersects(tempObject.getBounds())){
                   addBombDemage();
                 //  System.out.println("max demage "+GetBombDemage());
                }
            if(tempObject.getId() == ID.GiftKick)
                if(getBounds().intersects(tempObject.getBounds())){
                    SetKickBomb(true);
                  //  System.out.println("kick true");
                }
            if(tempObject.getId() == ID.GiftSpeed)
                if(getBounds().intersects(tempObject.getBounds())){
                   addSpeed();
                //   System.out.println("speed "+GetSpeed());
                }
            if(tempObject.getId() == ID.GiftThrowBomb)
                if(getBounds().intersects(tempObject.getBounds())){
                   setThrowBomb(true);
                //   System.out.println("throwBomb true");
                }
        }
        
    }
}