package bomberman;

import javax.swing.ImageIcon;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public abstract class GameObject {

    protected int x, y;

    protected ID id;

    protected int velx = 0, vely = 0;

    protected int maxPlayerSpeed = 3;

    protected int playerspeed = 1;

    protected int maxBombOnGame = 1;

    protected int bombDemage = 1;

    protected int lives = 3;

    protected boolean wonTheGame = false;

    protected boolean runOutOfTime = false;

    protected boolean mortality = true;

    protected int time = 0;

    protected int direction;

    protected boolean throwBomb = false;

    protected boolean isPlayerOnABomb;

    protected int bombSpeed = maxPlayerSpeed;

    protected int currentlyBombOnGame = 0;

    protected boolean kickBomb = false;

    protected boolean explodeByOtherBomb = false;

    protected boolean dead = false;

    protected boolean isItBomb = false;

    public int getImageTime() {
        return imageTime;
    }

    public void setImageTime(int imageTime) {

        this.imageTime = imageTime;
    }

    public int getImageTime2() {
        return imageTime2;
    }

    public void setImageTime2(int imageTime2) {
        this.imageTime2 = imageTime2;
    }

    private int imageTime = 0;

    private int imageTime2 = 1;

    private int imageCounter = 1;

    public abstract void tick();

    public abstract void render(Graphics g);

    public abstract Rectangle getBounds();

    public void drawGiftImage(Graphics g, int width, int heigth) {
        Image img = new ImageIcon("images/gifts.png").getImage();
        g.drawImage(img, x, y, x + 30, y + 30, width * 30, heigth * 30, 30 * width + 30, heigth * 30 + 30, null);
    }

    public void drawBlockImage(Graphics g, int width) {
        Image img = new ImageIcon("images/blocks.png").getImage();
        g.drawImage(img, x, y, x + 30, y + 30, width * 30, 0, 30 * width + 30, 30, null);
    }

    public void drawPlayerImage(Graphics g, int width, int heigth) {
        Image img = new ImageIcon("images/player.png").getImage();
        g.drawImage(img, x, y, x + 30, y + 30, width * 30, heigth * 30, 30 * width + 30, heigth * 30 + 30, null);
    }

    public void drawBombImage(Graphics g, int width, int heigth) {
        int a = 0;
        if (y < Game.w) {
            a = Game.w - y;
        }

        Image img = new ImageIcon("images/bomb.png").getImage();
        g.drawImage(img, x, y + a, x + 30, y + 30, width * 30, heigth * 30 + a, 30 * width + 30, heigth * 30 + 30, null);
    }

    public void drawBombExImage(Graphics g, int width, int heigth) {
        Image img = new ImageIcon("images/bombex.png").getImage();
        g.drawImage(img, x, y, x + 30, y + 30, width * 30, heigth * 30, 30 * width + 30, heigth * 30 + 30, null);
    }

    public void drawEnamyImage(Graphics g, int width, int heigth) {
        Image img = new ImageIcon("images/enamy.png").getImage();
        g.drawImage(img, x, y, x + 30, y + 30, width * 30, heigth * 30, 30 * width + 30, heigth * 30 + 30, null);
    }

    public void drawHeaderImage(Graphics g) {
        Image img = new ImageIcon("images/header.png").getImage();
        g.drawImage(img, 0, 0, Game.WIDTH - 10, Game.w, null);
    }

    public void drawNumberImage(Graphics g, int width, int xx, int yy) {
        Image img = new ImageIcon("images/numbers.png").getImage();
        g.drawImage(img, xx, yy, xx + 15, yy + 30, width * 15, 0, 15 * width + 15, 30, null);
    }

    public void drawClockImage(Graphics g, int width, int xx, int yy) {
        Image img = new ImageIcon("images/clock.png").getImage();
        g.drawImage(img, xx, yy, xx + 18, yy + 17, width * 18, 0, 18 * width + 18, 17, null);
    }

    public GameObject(int x, int y, ID id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public GameObject(ID id) {
        this.id = id;
    }

    public void SetX(int x) {
        this.x = x;
    }

    public void SetY(int y) {
        this.y = y;
    }

    public void SetId(ID id) {
        this.id = id;
    }

    public int GetX() {
        return x;
    }

    public int GetY() {
        return y;
    }

    public ID getId() {
        return id;
    }

    public boolean getRunOutOfTime() {
        return runOutOfTime;
    }

    public void setRunOutOfTime(boolean x) {
        runOutOfTime = x;
    }

    public void setLives(int live) {
        if (lives > 0) {
            lives += live;
        }
    }

    public void setMortality(boolean x) {
        mortality = x;
    }

    public boolean getMortality() {
        return mortality;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int x) {
        time = x;
    }

    public void addTime() {
        time++;
    }

    public boolean getWonTheGame() {
        return wonTheGame;
    }

    public void setWonTheGame(boolean x) {
        wonTheGame = x;
    }

    public int getLives() {
        return lives;
    }

    public boolean getThrowBomb() {
        return throwBomb;
    }

    public boolean getIsPlayerOnABomb() {
        return isPlayerOnABomb;
    }

    public void setIsPlayerOnABomb(boolean a) {
        isPlayerOnABomb = a;
    }

    public boolean getDead() {
        return dead;
    }

    public void setDead(boolean a) {
        dead = a;
    }

    public void nullImagetime() {
        imageTime2 = 0;
        imageCounter = 1;
    }

    public boolean getExplodeByOtherBomb() {
        return explodeByOtherBomb;
    }

    public void setExplodeByOtherBomb(boolean a) {
        explodeByOtherBomb = a;
    }

    public void setThrowBomb(boolean a) {
        throwBomb = a;
    }

    public void SetVelx(int velx) {
        this.velx = velx;
    }

    public void SetVely(int vely) {
        this.vely = vely;
    }

    public void SetMaxBombOnGame(int max) {
        this.maxBombOnGame = max;
    }

    public void AddBomb() {
        this.currentlyBombOnGame++;
    }

    public void RemoveBomb() {
        this.currentlyBombOnGame--;
    }

    public int GetVelx() {
        return velx;
    }

    public int GetVely() {
        return vely;
    }

    public void SetBombDemage(int demage) {
        this.bombDemage = demage;
    }

    public void setDirection(int d) {
        this.direction = d;
    }

    public int getDirection() {
        return direction;
    }

    public void addBombDemage() {
        bombDemage++;
    }

    public int GetBombDemage() {
        return bombDemage;
    }

    public void SetKickBomb(boolean kick) {
        this.kickBomb = kick;
    }

    public boolean GetKickBomb() {
        return kickBomb;
    }

    public void SetSpeed(int speed) {
        this.playerspeed = speed;
    }

    public boolean addSpeed() {
        if (maxPlayerSpeed > playerspeed) {
            playerspeed++;
            return true;
        } else {
            return false;
        }
    }

    public int GetSpeed() {
        return playerspeed;
    }

    public void SetBombSpeed(int speed) {
        this.bombSpeed = speed;
    }

    public int getBombSpeed() {
        return bombSpeed;
    }

    public int GetMaxBombOnGame() {
        return maxBombOnGame;
    }

    public void addMaxBombOnGame() {
        maxBombOnGame++;
    }

    public int GetCurrentlyBombOnGame() {
        return currentlyBombOnGame;
    }

    public int getAnimation(int time) {

        if (velx != 0 || vely != 0 || getDead() || isItBomb || wonTheGame || getRunOutOfTime()) {
            imageTime++;
            if (imageTime * ((wonTheGame || runOutOfTime) ? 1 : GetSpeed()) >= time) {
                imageTime2 += imageCounter;
                imageTime = 0;
                if (imageTime2 == 2 || imageTime2 == 0) {
                    imageCounter *= -1;
                }
            }
        }

        return imageTime2 + getAnimationById();
    }

    public int getAnimationById() {
        if (getId() == ID.Player || getId() == ID.Enemy) {
            return 0;
        } else if (getId() == ID.Player2) {
            return 3;
        }
        return 0;
    }

    public int getDeadAnimation(Handler handler) {
        if (imageCounter < 0 && imageTime2 == 1) {
            handler.remuveObject(this);
        }
        SetSpeed(1);
        return getAnimation(400);
    }
}
