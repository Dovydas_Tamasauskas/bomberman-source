package view.group;

import bomberman.Game;
import bomberman.Window;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import level_editor.Editor;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;
import view.components.CustomVBox;

/**
 * Creates map window layout as a Group object
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class MapGroup extends Group {
    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height= new SimpleDoubleProperty(600);

    /**
     * navigation window constructor for map loading selection
     */
    public MapGroup(DoubleProperty width, DoubleProperty height) {
        this.width = width;
        this.height = height;
        String[] buttonNames = {"Generate Map", "Choose Map", "Load Drawn Map", "Create Map", "Back"};
        Button[] buttons = setupButtons(buttonNames, 300);
        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), CustomVBox.setupVBox(buttons,width.get()),CustomButtons.exitButton(width,height));
    }

    /**
     * set up array of buttons the size of array of string button names
     * each button is assigned corresponding string as button name
     * @param buttonNames array of button names
     * @param w button width
     * @return array of buttons
     */
    private Button[] setupButtons(String[] buttonNames, double w) {
        Button[] btnGroup = new Button[buttonNames.length];
        int i = 0;
        for (String s : buttonNames) {
            btnGroup[i] = CustomButtons.basicButton(s, w);
            btnGroup[i].setOnAction(this::onButtonClick);
            btnGroup[i].setId("mapG_"+s.split(" ")[0]);
            i++;
        }
        return btnGroup;
    }

    /**
     * button event handler
     * @param event button click
     */
    private void onButtonClick(ActionEvent event) {
        switch (((Button) event.getSource()).getText()) {
            case ("Choose Map"):
                MainWindow.chooseScene(new ChooseMapGroup(width,height,ChooseMapGroup.SINGLE_PLAYER));
                break;
            case ("Load Drawn Map"):
                MainWindow.chooseScene(new ChooseImageGroup(width,height));
                break;
            case ("Create Map"):
                new Editor();
                break;
            case ("Back"):
                MainWindow.chooseScene(new MainGroup(width,height));
                break;
            case ("Generate Map"):
                new Game();
                Platform.exit();
                break;
            default:
                break;
        }
    }

}
