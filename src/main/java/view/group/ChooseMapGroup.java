package view.group;

import bomberman.Game;
import database.utility.MapData;
import database.utility.QueryExecutor;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import level_editor.Editor;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;
import view.components.CustomHBox;

import java.util.List;

/**
 * Creates choose map window layout as a Group object
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class ChooseMapGroup extends Group {

    private List<MapData> mapList;
    public static final int MULTIPLAYER = 1;
    public static final int SINGLE_PLAYER = 2;
    private static int fromStage = 0;

    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height = new SimpleDoubleProperty(600);

    private MapData selectedMap;
    private Button buttons[];

    /**
     * map choice window for database map selection
     *
     * @param fromStage window from which arrived
     */
    public ChooseMapGroup(DoubleProperty width, DoubleProperty height, int fromStage) {
        this.width = width;
        this.height = height;
        ChooseMapGroup.fromStage = fromStage;
        String[] buttonNames = {"Edit", "Load", "Cancel"};
        mapList = QueryExecutor.selectMapDataFromDatabase();
        buttons = setupButtons(buttonNames, 150);

        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), setupVBox(CustomHBox.setupHBox(buttons), mapView()),CustomButtons.exitButton(width,height));
    }

    /**
     * setup ListView for choosing from available map list
     *
     * @return - ListView component with available map names
     */

    public ListView<MapData> mapView() {
        ObservableList<MapData> tmpList2 = FXCollections.observableArrayList();
        tmpList2.addAll(mapList);
        final ListView<MapData> tmpList = new ListView<>(tmpList2);
        tmpList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null){
                selectedMap = newValue;
                for(int i = 0; i<buttons.length;i++){
                    if(buttons[i].getText() != "Cancel"){
                        buttons[i].setDisable(false);
                    }
                }
            }
        });
        tmpList.setPrefWidth(200);
        tmpList.setMaxWidth(400);
        tmpList.setPrefHeight(400);
        tmpList.setMinHeight(400);
        tmpList.setCellFactory(new Callback<ListView<MapData>, ListCell<MapData>>() {
            @Override
            public ListCell<MapData> call(ListView<MapData> param) {
                return new ListCell<MapData>() {
                    @Override
                    public void updateItem(MapData item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.toString());
                        }
                    }
                };
            }
        });
        return tmpList;
    }

    /**
     * adds map name to the list of available maps
     *
     * @param mapToAdd - name of the map to add
     * @return - last name of the map on the list
     */
    public MapData addMap(MapData mapToAdd) {
        mapList.add(mapToAdd);
        return mapList.get(mapList.size() - 1);
    }

    /**
     * setup button for each string object in array and adds action listener
     *
     * @param buttonNames - string array of button names
     * @return setup button array
     */
    private Button[] setupButtons(String[] buttonNames, double w) {
        Button[] btnGroup = new Button[buttonNames.length];
        int i = 0;
        for (String s : buttonNames) {
            btnGroup[i] = CustomButtons.basicButton(s, w);
            if (fromStage == MULTIPLAYER && s.equals("Edit")) {
                btnGroup[i].setVisible(false);
                btnGroup[i].setTextFill(Color.GRAY);
            }
            if(!s.equals("Cancel")){
                btnGroup[i].setDisable(true);
            }
            btnGroup[i].setId("chooseMG_"+s);
            btnGroup[i].setOnAction(this::onChangeScene);
            i++;
        }
        return btnGroup;
    }


    /**
     * setup actions to take when button is clicked
     * different actions on different buttons
     *
     * @param event - button event (button clicked)
     */
    private void onChangeScene(ActionEvent event) {
        switch (((Button) event.getSource()).getText()) {
            case ("Edit"):
                new Editor(selectedMap);
                Platform.exit();
                break;
            case ("Load"):
                new Game(selectedMap);
                Platform.exit();
                break;
            case ("Cancel"):
                switch (fromStage) {
                    case (MULTIPLAYER):
                        MainWindow.chooseScene(new MultiplayerSettingsGroup(width,height));
                        break;
                    case (SINGLE_PLAYER):
                        MainWindow.chooseScene(new MapGroup(width,height));
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    /**
     * setup VBox with list of available maps and edit, load and cancel buttons
     *
     * @param hBox     - HBox containing button row below the list
     * @param listView - list containing available map names
     * @return - VBox layout
     */
    public static VBox setupVBox(HBox hBox, ListView<MapData> listView) {
        VBox vBox = new VBox(5);
        vBox.setPadding(new Insets(20));
        vBox.setSpacing(20);
        vBox.setPrefWidth(800);
        vBox.setPrefHeight(600);
        vBox.getChildren().addAll(listView, hBox);
        vBox.alignmentProperty().set(Pos.TOP_CENTER);
        return vBox;
    }

}
