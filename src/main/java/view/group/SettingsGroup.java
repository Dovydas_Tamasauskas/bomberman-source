package view.group;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Creates settings window layout as a Group object
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class SettingsGroup extends Group {

    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height = new SimpleDoubleProperty(600);
    private String[] settings = new String[5];

    private static final Logger log = Logger.getLogger(ChooseImageGroup.class.getName());

    /**
     * constructor for this layout
     * @param width window width
     * @param height window height
     */
    public SettingsGroup(DoubleProperty width, DoubleProperty height) {
        this.width = width;
        this.height = height;
        try (BufferedReader br = new BufferedReader(new FileReader(new File("custom.settings")))) {
            String line;
            for (int i = 0; i < 5; i++) {
                line = br.readLine();
                settings[i] = line;
            }
        } catch (FileNotFoundException e) {
            log.log(Level.FINE, "no settings file found");
        } catch (IOException e) {
            log.log(Level.FINE, "can not write to settings file");
        }

        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), setupView(), CustomButtons.exitButton(width, height), setupButtons()[0], setupButtons()[1]);

    }

    /**
     * setup save and back buttons
     * @return button array
     */
    private Button[] setupButtons() {
        Button[] btn = new Button[2];
        Button saveButton = CustomButtons.basicButton("Save", width.get() * .2);
        saveButton.setLayoutY(height.get() - 100);
        saveButton.setLayoutX(width.get() * .7);
        saveButton.setOnAction(event -> {
            for(String s:settings){
                if(s == null){
                    return;
                }
            }
            printSettings();
            width.set(Integer.valueOf(settings[4].split(" ")[0]));
            height.set(Integer.valueOf(settings[4].split(" ")[1]));
            MainWindow.chooseScene(new MainGroup(width, height));
        });
        Button backButton = CustomButtons.basicButton("Back", width.get() * .2);
        backButton.setLayoutY(height.get() - 100);
        backButton.setLayoutX(width.get() * .4);
        backButton.setOnAction(event -> MainWindow.chooseScene(new MainGroup(width, height)));
        btn[0] = saveButton;
        btn[1] = backButton;
        return btn;
    }

    /**
     * print customized settings to file
     */
    private void printSettings() {
        File file = new File("custom.settings");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(settings[0]);
            bw.newLine();
            bw.write(settings[1]);
            bw.newLine();
            bw.write(settings[2]);
            bw.newLine();
            bw.write(settings[3]);
            bw.newLine();
            bw.write(settings[4]);
            bw.close();
        } catch (IOException e) {
            log.log(Level.WARNING,"Error while saving settings");
        }
    }

    /**
     * setup folding view
     * @return Accordion object
     */
    private Accordion setupView() {
        Accordion accordion = new Accordion();
        accordion.setLayoutX(50);
        accordion.setLayoutY(50);
        accordion.setPrefWidth(width.get() * .8);
        TitledPane colorPane = new TitledPane("Color setup", setupSliders());
        colorPane.setLayoutY(20);
        colorPane.setLayoutX(20);
        TitledPane sizePane = new TitledPane("Screen Size", setupScreenSize());
        accordion.getPanes().addAll(colorPane, sizePane);


        return accordion;
    }

    /**
     * setup sliders for color and opacity selection
     * @return VBox layout object
     */
    private VBox setupSliders() {
        HBox redSliderBox = new HBox(20);
        redSliderBox.setPadding(new Insets(20));
        Label redLabel = new Label("Red");
        redLabel.setPrefWidth(50);
        Slider redSlider = new Slider(0, 255, 255);
        DoubleProperty redProperty = new SimpleDoubleProperty(255);
        redProperty.bind(redSlider.valueProperty());
        redSlider.setPrefWidth(width.get() * .6);
        redSlider.setShowTickLabels(true);
        redSlider.setShowTickMarks(true);
        redSlider.setMajorTickUnit(50);
        redSlider.setMinorTickCount(5);
        redSlider.setBlockIncrement(1);
        redSliderBox.getChildren().addAll(redLabel, redSlider);

        HBox greenSliderBox = new HBox(20);
        greenSliderBox.setPadding(new Insets(20));
        Label greenLabel = new Label("Green");
        greenLabel.setPrefWidth(50);
        Slider greenSlider = new Slider(0, 255, 255);
        DoubleProperty greenProperty = new SimpleDoubleProperty(255);
        greenProperty.bind(greenSlider.valueProperty());
        greenSlider.setPrefWidth(width.get() * .6);
        greenSlider.setShowTickLabels(true);
        greenSlider.setShowTickMarks(true);
        greenSlider.setMajorTickUnit(50);
        greenSlider.setMinorTickCount(5);
        greenSlider.setBlockIncrement(1);
        greenSliderBox.getChildren().addAll(greenLabel, greenSlider);

        HBox blueSliderBox = new HBox(20);
        blueSliderBox.setPadding(new Insets(20));
        Label blueLabel = new Label("Blue");
        blueLabel.setPrefWidth(50);
        Slider blueSlider = new Slider(0, 255, 255);
        DoubleProperty blueProperty = new SimpleDoubleProperty(255);
        blueProperty.bind(blueSlider.valueProperty());
        blueSlider.setPrefWidth(width.get() * .6);
        blueSlider.setShowTickLabels(true);
        blueSlider.setShowTickMarks(true);
        blueSlider.setMajorTickUnit(50);
        blueSlider.setMinorTickCount(5);
        blueSlider.setBlockIncrement(1);
        blueSliderBox.getChildren().addAll(blueLabel, blueSlider);

        HBox opacitySliderBox = new HBox(20);
        opacitySliderBox.setPadding(new Insets(20));
        Label opacityLabel = new Label("Opacity");
        opacityLabel.setPrefWidth(50);
        Slider opacitySlider = new Slider(0, 1, 1);
        DoubleProperty opacityProperty = new SimpleDoubleProperty(0);
        opacityProperty.bind(opacitySlider.valueProperty());
        opacitySlider.setPrefWidth(width.get() * .6);
        opacitySlider.adjustValue(1);
        opacitySlider.setShowTickLabels(true);
        opacitySlider.setShowTickMarks(true);
        opacitySlider.setMajorTickUnit(.1);
        opacitySlider.setBlockIncrement(.01);
        opacitySliderBox.getChildren().addAll(opacityLabel, opacitySlider);

        VBox colorGroup = new VBox();
        redSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorGroup.setBackground(new Background(new BackgroundFill(Color.rgb((int) redProperty.get(), (int) greenProperty.get(), (int) blueProperty.get(), opacityProperty.get()), new CornerRadii(1), null)));
            settings[0] = Integer.toString((int) redProperty.get());
        });
        greenSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorGroup.setBackground(new Background(new BackgroundFill(Color.rgb((int) redProperty.get(), (int) greenProperty.get(), (int) blueProperty.get(), opacityProperty.get()), new CornerRadii(1), null)));
            settings[1] = Integer.toString((int) greenProperty.get());
        });
        blueSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorGroup.setBackground(new Background(new BackgroundFill(Color.rgb((int) redProperty.get(), (int) greenProperty.get(), (int) blueProperty.get(), opacityProperty.get()), new CornerRadii(1), null)));
            settings[2] = Integer.toString((int) blueProperty.get());
        });
        opacitySlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            colorGroup.setBackground(new Background(new BackgroundFill(Color.rgb((int) redProperty.get(), (int) greenProperty.get(), (int) blueProperty.get(), opacityProperty.get()), new CornerRadii(1), null)));
            settings[3] = String.format("%.5g", opacityProperty.get());
        });
        colorGroup.minWidth(width.get() * .8);
        colorGroup.minHeight(height.get() * .6);
        colorGroup.getChildren().addAll(redSliderBox, greenSliderBox, blueSliderBox, opacitySliderBox);
        return colorGroup;
    }

    /**
     * setup drop down list for screen size selection
     * @return HBox view as Node object
     */
    private Node setupScreenSize() {
        HBox hBox = new HBox(10);
        ObservableList<String> sizeList = FXCollections.observableArrayList();
        String list[] = {"800 x 600", "1024 x 768", "1152 x 864", "1280 x 720", "1280 x 768", "1280 x 800", "1280 x 960", "1280 x 1024", "1366 x 768", "1600 x 900", "1600 x 1024", "1680 x 1050", "1920 x 1080"};
        sizeList.addAll(list);
        ComboBox<String> sizeBox = new ComboBox<>(sizeList);
        sizeBox.getSelectionModel();
        sizeBox.setOnAction(event -> {
            settings[4] = sizeBox.getValue().split(" x ")[0] + " " + sizeBox.getValue().split(" x ")[1];
        });
        Label sizeLabel = new Label("Select window size");
        hBox.getChildren().addAll(sizeLabel, sizeBox);
        return hBox;
    }
}
