package view.group;

import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;
import view.components.CustomHBox;
import view.components.CustomTables;

/**
 * Creates multiplayer window layout as a Group object
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class MultiplayerSettingsGroup extends Group {

    private static StringProperty ip;
    private static BooleanProperty enabled;
    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height= new SimpleDoubleProperty(800);

    /**
     * constructor for creating group object
     */
    public MultiplayerSettingsGroup(DoubleProperty width, DoubleProperty height) {
this.width = width;
        this.height= height;
        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), setupVBox(),CustomButtons.exitButton(width,height));
    }

    public static final BooleanProperty enabledProperty() {
        if (enabled == null) {
            enabled = new SimpleBooleanProperty(false);
        }
        return enabled;
    }

    public static boolean getEnabled() {
        return enabledProperty().get();
    }

    public static void setEnabled(boolean enabled) {
        enabledProperty().set(enabled);
    }

    public static final StringProperty ipProperty() {
        if (ip == null) {
            ip = new SimpleStringProperty();
        }
        return ip;
    }

    public static final String getIP() {
        return ipProperty().get();
    }

    public static final void setIp(String ip) {
        ipProperty().set(ip);
    }

    /**
     * setup vertical box with connect, host and cancel buttons
     *
     * @return VBox object with 3 buttons
     */
    private VBox setupVBox() {
        Button[] btn = {CustomButtons.basicButton("Connect", 150), CustomButtons.basicButton("Host", 100), CustomButtons.basicButton("Cancel", 150)};
        for (Button aBtn : btn) {
            aBtn.setOnAction(this::handler);
            aBtn.setId("multiG_"+aBtn.getText().split(" ")[0]);
        }
        VBox vBox = new VBox(5);
        vBox.setPadding(new Insets(50));
        vBox.setSpacing(50);
        vBox.setPrefWidth(width.get());
        vBox.setPrefHeight(600);
        vBox.getChildren().addAll(CustomTables.connectionTable(), CustomHBox.setupHBox(btn));
        vBox.alignmentProperty().set(Pos.TOP_CENTER);
        return vBox;
    }

    /**
     * method to handle button events
     *
     * @param e - action event (here used to handle button clicks)
     */
    private void handler(javafx.event.ActionEvent e) {
        switch (((Button) e.getSource()).getText()) {
            case ("Host"):
                MainWindow.chooseScene(new ChooseMapGroup(width,height,ChooseMapGroup.MULTIPLAYER));
                break;
            case ("Cancel"):
                MainWindow.chooseScene(new MainGroup(width,height));
                break;
            default:
                break;
        }
    }
}
