package view.group;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import view.MainWindow;
import view.components.CustomBackgrounds;
import view.components.CustomButtons;
import view.components.CustomVBox;

import java.util.logging.Logger;

/**
 * Creates main window layout as a Group object
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class MainGroup extends Group {

    private DoubleProperty width = new SimpleDoubleProperty(800);
    private DoubleProperty height = new SimpleDoubleProperty(600);


    /**
     * btnGroup - setup button array for UI
     */
    public MainGroup(DoubleProperty width, DoubleProperty height) {
        this.width = width;
        this.height = height;
        String[] buttonNames = {"Single Player", "Multiplayer", "Settings", "About", "Exit"};
        Button[] btnGroup = setupButtons(buttonNames, 300);
        this.getChildren().addAll(CustomBackgrounds.ovalRectangleBg(width, height), CustomVBox.setupVBox(btnGroup,width.get()),CustomButtons.exitButton(width,height));
    }

    /**
     * @param buttonNames - button name string array
     * @return - setup button object array
     */
    private Button[] setupButtons(String[] buttonNames, double w) {
        Button[] btnGroup = new Button[buttonNames.length];
        int i = 0;
        for (String s : buttonNames) {
            btnGroup[i] = CustomButtons.basicButton(s, w);
            btnGroup[i].setId("mainG_"+s.split(" ")[0]);
            btnGroup[i].setOnAction(this::onChangeScene);
            i++;
        }
        return btnGroup;
    }


    /**
     * setup actions to take when button is clicked
     * different actions on different buttons
     *
     * @param event - button event (button clicked)
     */
    private void onChangeScene(ActionEvent event) {
        switch (((Button) event.getSource()).getText()) {
            case ("Exit"):
                Platform.exit();
                break;
            case ("Single Player"):
                MainWindow.chooseScene(new MapGroup(width,height));
                break;
            case ("Multiplayer"):
                MainWindow.chooseScene(new MultiplayerSettingsGroup(width,height));
                break;
            case ("About"):
                MainWindow.chooseScene(new AboutGroup(width,height));
                break;
            case ("Settings"):
                MainWindow.chooseScene(new SettingsGroup(width,height));
                break;
            default:
                break;
        }
    }
}
