package view.components;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * Class for customized vertical boxes
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class CustomVBox {
    /**
     * set up VBox layout to add to group
     *
     * @param btn - button array to add to VBox
     * @return - setup VBox layout object
     */
    public static VBox setupVBox(Button[] btn,double width) {
        VBox vBox = new VBox(5);
        vBox.setPadding(new Insets(50));
        vBox.setSpacing(50);
        vBox.setPrefWidth(width);
        vBox.getChildren().addAll(btn);
        vBox.alignmentProperty().set(Pos.BASELINE_CENTER);

        return vBox;
    }
}
