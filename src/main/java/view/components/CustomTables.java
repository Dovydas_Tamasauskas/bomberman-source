package view.components;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import models.ConnectionInfo;
import view.group.MultiplayerSettingsGroup;

/**
 * Class for customized tables
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class CustomTables {

    /**
     * TableView object with three columns for name, map and size
     * user select row for connection purposes
     *
     * @return TableView object for Multiplayer Settings window
     */
    public static TableView connectionTable() {

        final ObservableList<ConnectionInfo> connectionList = getHostList();
        final TableView<ConnectionInfo> connectionTableView = new TableView<>(connectionList);
        connectionTableView.setPrefWidth(600);
        connectionTableView.setPrefHeight(400);

        TableColumn<ConnectionInfo, String> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory("name"));
        column1.setPrefWidth(connectionTableView.getPrefWidth() / 3);
        column1.setEditable(true);
        TableColumn<ConnectionInfo, String> column2 = new TableColumn<>("Map");
        column2.setCellValueFactory(new PropertyValueFactory("map"));
        column2.setPrefWidth(connectionTableView.getPrefWidth() / 3);
        TableColumn<ConnectionInfo, String> column3 = new TableColumn<>("Size");
        column3.setCellValueFactory(new PropertyValueFactory("size"));
        column3.setPrefWidth(connectionTableView.getPrefWidth() / 3);
        connectionTableView.getColumns().addAll(column1, column2, column3);
        connectionList.addAll(getHostList());

        connectionTableView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ConnectionInfo> observable, ConnectionInfo oldValue, ConnectionInfo newValue) -> {
            if (observable != null && observable.getValue() != null) {
                MultiplayerSettingsGroup.setIp(observable.getValue().getIP() + ":" + observable.getValue().getPort());
                MultiplayerSettingsGroup.setEnabled(true);
            }
        });

        VBox.setVgrow(connectionTableView, Priority.ALWAYS);
        return connectionTableView;
    }

    /**
     * Method to get host list for connection from database
     * needs to be updated to use database data when available
     *
     * @return ObservableList object with host data
     */
    private static ObservableList<ConnectionInfo> getHostList() {

        //static content needs to be changed to data received from database on availability
        final ObservableList<ConnectionInfo> connectionList = FXCollections.observableArrayList();
        ConnectionInfo host1 = new ConnectionInfo("xxx.xx.xx.xxx", "Map1", "20x20", "Host1", 8080);
        ConnectionInfo host2 = new ConnectionInfo("xxx.xx.xx.xxx", "Map2", "10x10", "Host2", 7080);
        ConnectionInfo host3 = new ConnectionInfo("xxx.xx.xx.xxx", "Map1", "10x20", "Host3", 9080);
        ConnectionInfo host4 = new ConnectionInfo("xxx.xx.xx.xxx", "Map3", "20x15", "Host4", 80);
        connectionList.addAll(host1, host2, host3, host4);

        return connectionList;
    }
}
