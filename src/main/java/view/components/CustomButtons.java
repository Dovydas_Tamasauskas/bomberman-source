package view.components;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Button;

/**
 * Class for customized button design
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class CustomButtons {

    /**
     * setup button which on click close screen
     * @param screenWidth width of screen
     * @param screenHeight height of screen
     * @return exit button at the top right corner of the screen
     */
    public static Button exitButton(DoubleProperty screenWidth, DoubleProperty screenHeight){
        Button exitButton = new Button("X");
        exitButton.getStyleClass().add("exit-button");
        exitButton.setLayoutY(screenHeight.get() * .02);
        exitButton.setLayoutX(screenWidth.get() * .96);
        exitButton.setOnAction(event -> Platform.exit());
        return exitButton;
    }

    /**
     * setup common button
     * @param buttonText text shown on the button
     * @param width button width
     * @return button with specified width and text
     */
    public static Button basicButton(String buttonText, double width) {
        if (buttonText == null) {
            buttonText = "NULL";
        }

        Button btn = new Button(buttonText);
        btn.getStyleClass().add("custom-button");
        btn.setPrefWidth(width);
        return btn;
    }
}
