package view.components;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * Created by Aivaro on 18/04/2016.
 */
public class CustomHBox {
    /**
     * setup HBox layout for buttons
     *
     * @param btn - edit, load and cancel navigation buttons
     * @return list of setup buttons
     */
    public static HBox setupHBox(Button[] btn) {
        HBox hBox = new HBox(5);
        hBox.setPadding(new Insets(50));
        hBox.setSpacing(30);
        hBox.setPrefWidth(600);
        hBox.getChildren().addAll(btn);
        hBox.alignmentProperty().set(Pos.TOP_CENTER);
        return hBox;
    }
}
