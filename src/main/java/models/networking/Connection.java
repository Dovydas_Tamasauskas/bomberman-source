package models.networking;

import services.MultiplayerServices;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class for network connectivity
 *
 * @author Aivaras Dziaugys
 * @version 1.0
 */
public class Connection {
    private static ObjectOutputStream outputStream;
    private static ObjectInputStream inputStream;
    private static Socket connection;
    private String message = "";

    /**
     * connect to server/client
     *
     * @param socket connection socket
     */
    public void setConnection(Socket socket) {
        connection = socket;
    }

    /**
     * close input/output streams
     */
    public void closeConnection() {
        try {
            inputStream.close();
            outputStream.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * opens communication streams
     *
     * @throws IOException
     */
    public void setupStreams() throws IOException {
        outputStream = new ObjectOutputStream(connection.getOutputStream());
        outputStream.flush();
        inputStream = new ObjectInputStream(connection.getInputStream());
        //TODO  show that connection has been made
    }

    /**
     * enables streaming data while connection is set up
     *
     * @throws IOException
     */
    public void whileConnected() throws IOException {
        String data = "CONNECTION ESTABLISHED";
        sendData(data);
        do {
            try {
                message = (String) inputStream.readObject();
                decode(message);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } while (!message.equals("END CONNECTION"));

    }

    /**
     * send data to remote server/client
     *
     * @param data - string to send
     */
    public void sendData(String data) {
        try {
            outputStream.writeObject(data);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * transfer received data to multiplayer services for further actions
     *
     * @param data
     */
    private void decode(String data) {
        MultiplayerServices.decode(data);
    }
}
