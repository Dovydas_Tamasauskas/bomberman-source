package opencvModule.dataStructure;

import org.opencv.core.Size;

/**
 * Created by Saulius Stankevičius.
 * Generic data structure for matrix
 */
@SuppressWarnings("unchecked")
public class Matrix<T>
{
    private Object[][] matrix;
    private int cols = 0;
    private int rows = 0;
    private int size = 0;

    public String testMethod() { return "HERE"; }

    /**
     * Constructor for the Matrix class
     * @param size size of the matrix
     */
    public Matrix(Size size)
    {
        this.cols = (int) size.width;
        this.rows = (int) size.height;
        this.matrix = new Object[rows][cols];
    }

    /**
     * Constructor for the Matrix class
     * @param cols width of the matrix
     * @param rows height of the matrix
     */
    public Matrix(int cols, int rows)
    {
        this.matrix = new Object[rows][cols];
        this.cols = cols;
        this.rows = rows;
    }

    /**
     * Returns the matrix
     * @return matrix
     */
    public T[][] getMatrix()
    {
        return (T[][])this.matrix;
    }

    /**
     * Adds the value to the given place
     * @param row y
     * @param col x
     * @param value value to add
     */
    public void add(int row, int col, T value)
    {
        if(row > this.rows || col > this.cols)
        {
            throw new ArrayIndexOutOfBoundsException("Matrix<T> overflow");
        }

        this.matrix[row][col] = value;
        this.size++;
    }

    /**
     * Adds the value at the end, if the place is not empty finds the first empty space
     * @param value to add
     */
    public void add(T value)
    {
        if(size > this.cols * this.rows)
        {
            throw new ArrayIndexOutOfBoundsException("Matrix<T> overflow");
        }

        int y = this.size / this.rows;
        int x = this.size - (y * (this.cols + 1));

        if(this.matrix[y][x] == null)
        {
            this.add(y, x, value);
        }
        else
        {
            this.addToFirstEmptySpace(value);
        }
    }

    /**
     * Add the value to the first found empty place
     * @param value to add
     */
    private void addToFirstEmptySpace(T value)
    {
        for(int y = 0; y < this.rows; y++)
        {
            for(int x = 0; x < this.cols; x++)
            {
                if(this.matrix[y][x] == null)
                {
                    this.add(y, x, value);
                }
            }
        }
    }

    /**
     * Returns needed value
     * @param x coordinate
     * @param y coordinate
     * @return value
     */
    public T get(int x, int y)
    {
        return (T)this.matrix[y][x];
    }

    /**
     * Returns semi formatted matrix string e.g.
     * {{1, 0, 1}, {0, 1, 0}, {1, 0, 1}};
     * @return matrix string
     */
    public String toSemiFormattedString()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{");
        for(int y = 0; y < this.rows; y++)
        {
            stringBuilder.append("{");
            for(int x = 0; x < this.cols; x++)
            {
                if(this.matrix[y][x] != null) stringBuilder.append((T)this.matrix[y][x].toString());
                else stringBuilder.append("null");

                if(x != this.cols - 1) stringBuilder.append(",");
            }
            stringBuilder.append("}");
            if(y != this.rows - 1) stringBuilder.append(",");
        }
        stringBuilder.append("};");

        return stringBuilder.toString();
    }

    /**
     * Returns matrix string e.g.
     * 3 3 101010101
     * @return matrix string
     */
    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(this.cols).append(" ");
        stringBuilder.append(this.rows).append(" ");

        for(int y = 0; y < this.rows; y++)
        {
            for(int x = 0; x < this.cols; x++)
            {
                if(this.matrix[y][x] != null) stringBuilder.append((T)this.matrix[y][x].toString());
                else stringBuilder.append("0");
            }
        }

        return stringBuilder.toString();
    }

    /**
     * Returns fully formatted matrix string e.g.
     * 1 0 1
     * 0 1 0
     * 1 0 1
     * @return matrix string
     */
    public String toFormattedString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(int y = 0; y < this.rows; y++)
        {
            for(int x = 0; x < this.cols; x++)
            {
                if(this.matrix[y][x] != null) stringBuilder.append((T)this.matrix[y][x].toString()).append(" ");
                else stringBuilder.append("NULL").append(" ");
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    /**
     * Returns the dimensions of the matrix
     * @return dimensions
     */
    public int getSize()
    {
        return this.size;
    }

    /**
     * Returns matrix column count
     * @return column count
     */
    public int getCols() { return this.cols; }

    /**
     * Returns matrix rows count
     * @return rows count
     */
    public int getRows() { return this.rows; }
}
