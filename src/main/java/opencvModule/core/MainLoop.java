package opencvModule.core;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import opencvModule.imageOut.ImagePanelInterface;
import opencvModule.dataStructure.*;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by Saulius Stankevičius.
 * Class for looping main calculations
 */
public class MainLoop implements Callable
{
    private boolean run = false;
    private boolean isPhoto = false;

    private ImagePanelInterface imagePanel;

    private MatImage capturedMat = new MatImage();

    ArrayList<BufferedImage> imagesToOverlay = new ArrayList<>();

    /**
     * Constructor for the MainLoop class
     * @param imagePanel panel to display results
     * @param imagesToOverlay images to overlay on the captured image
     */
    public MainLoop(ImagePanelInterface imagePanel, ArrayList<BufferedImage> imagesToOverlay)
    {
        this.imagePanel = imagePanel;
        this.isPhoto = false;

        this.imagesToOverlay = imagesToOverlay;
    }

    /**
     * Constructor for the MainLoop class
     * @param image to analyze
     * @param imagePanel panel to display results
     * @param imagesToOverlay images to overlay on the captured image
     */
    public MainLoop(BufferedImage image, ImagePanelInterface imagePanel, ArrayList<BufferedImage> imagesToOverlay)
    {
        this.isPhoto = true;
        this.capturedMat = new MatImage(image);
        this.imagePanel = imagePanel;

        this.imagesToOverlay = imagesToOverlay;
    }

    /**
     * The main loop
     */
    public Matrix<Integer> run()
    {
        Matrix<Integer>matrix = null;
        if(this.isPhoto)
        {
            MatOfPoint gridContour = MainLoopCalculations.getGridContour(capturedMat);
            matrix = MainLoopCalculations.getCalculatedGridMatrix(capturedMat, gridContour);
            this.displayMatrix(matrix, capturedMat, gridContour);
        }
        else
        {
            VideoCapture videoCapture = VideoCaptureUtilities.startCapture();
            while(run)
            {
                if(videoCapture != null && videoCapture.isOpened())
                {
                    if(videoCapture.read(this.capturedMat))
                    {
                        MatOfPoint gridContour = MainLoopCalculations.getGridContour(capturedMat);
                        matrix = MainLoopCalculations.getCalculatedGridMatrix(capturedMat, gridContour);
                        this.displayMatrix(matrix, capturedMat, gridContour);
                    }
                }
            }
            VideoCaptureUtilities.stopCapture(videoCapture);
        }

        return matrix;
    }

    /**
     * Displays the matrix output on the ImagePanel
     * @param matrix to display
     * @param capturedMat to blend with
     * @param gridContour the contour of the grid
     */
    private void displayMatrix(Matrix<Integer> matrix, MatImage capturedMat, MatOfPoint gridContour)
    {
        if(matrix != null)
        {
            MatImage matImageToOverlay = MatImageProcessing.constructMatImageFromMatrix(matrix, imagesToOverlay);
            imagePanel.refreshImage(MatImageProcessing.getBlendedMatImage(capturedMat, matImageToOverlay, gridContour).getImage());
        }
        else
        {
            imagePanel.refreshImage(capturedMat.getImage());
        }
    }

    /**
     * Sets run state
     * @param run desired state
     */
    public void setRun(boolean run)
    {
        this.run = run;
    }

    /**
     * Returns current map string
     * @return map string
     */
    @Override
    public String call()
    {
        Matrix<Integer> output = this.run();

        if(output != null)
        {
            return output.toString();
        }

        return null;
    }
}
