package opencvModule.core;

import org.opencv.core .MatOfPoint;
import org.opencv.core.Point;

/**
 * Created by Saulius Stankevičius.
 * Class for recognizing a shape of a MatOfPoint
 */
public class ShapeRecognizer
{
    private ShapeRecognizer()
    {

    }

    /**
     * Checks if the given contour specified angle
     * @param contour to analyze
     * @param angle angle in degrees to look for
     * @param margin margin in degrees
     * @return true if it has, false - if not
     */
    public static boolean checkIfContainsAngle(MatOfPoint contour, int angle, int margin)
    {
        Point[] contourPoint = contour.toArray();

        for(int i = 2; i < contourPoint.length + 2; i++)
        {
            double angleInDegrees = getAngleInDegrees(contourPoint[i - 2],
                    i == contourPoint.length + 1 ? contourPoint[0] : contourPoint[i - 1], contourPoint[i % contourPoint.length]);
            if(angleInDegrees > angle - margin && angleInDegrees < angle + margin) return true;
        }

        return false;
    }

    /**
     * Calculates and returns an angle between 3 points it is assumed that points are in correct order a->b->c
     * @param a start point
     * @param b angle point
     * @param c end point
     * @return angle
     */
    public static double getAngleInDegrees(Point a, Point b, Point c)
    {
        double x1 = b.x - a.x;
        double y1 = b.y - a.y;

        double x2 = c.x - b.x;
        double y2 = c.y - b.y;

        double cosUpper = (x1 * x2) + (y1 * y2);
        double cosBottom = Math.sqrt(Math.pow(x1, 2) + Math.pow(y1, 2)) + Math.sqrt(Math.pow(x2, 2) + Math.pow(y2, 2));

        double cos = cosUpper / cosBottom;

        return Math.toDegrees(Math.acos(Math.toRadians(cos)));
    }

    /**
     * Recognizes a shape of the MatOfPoint and returns the result
     * @param contour contour
     * @return shape name if it can't be recognized returns null
     */
    public static String getShapeName(MatOfPoint contour)
    {
        MatOfPoint approximatedContour = ContourUtilities.getApproximatedContour(contour, 0.11);

        if(approximatedContour.toArray().length == 3) return "TRIANGLE";
        if(approximatedContour.toArray().length == 4)
        {
            if(checkIfContainsAngle(approximatedContour, 90, 10)) return "RECTANGLE";
            else return "TRIANGLE";
        }

        return null;
    }
}
