package opencvModule.core;

import opencvModule.dataStructure.Matrix;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import opencvModule.dataStructure.MatImage;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Saulius Stankevičius.
 * Class for easy processing of MatImages
 */
public class MatImageProcessing
{
    private MatImageProcessing()
    {

    }

    /**
     * Removes everything from the MatImage except what is inside of the contour, return MatImage has the same size as input
     * @param matImage MatImage to process
     * @param contour content to leave
     * @return processed MatImage
     */
    public static MatImage getOnlyContourContent(MatImage matImage, MatOfPoint contour)
    {
        if(matImage != null && contour.toArray().length > 2)
        {
            MatImage maskMatImage = new MatImage(matImage.size(), CvType.CV_8U, new Scalar(0, 0, 0));
            MatImage extractedMatImage = new MatImage();
            ArrayList<MatOfPoint> contourArray = new ArrayList<>();
            contourArray.add(contour);

            Imgproc.drawContours(maskMatImage, contourArray, -1, new Scalar(250, 250, 250), -1);
            matImage.copyTo(extractedMatImage, maskMatImage);

            return extractedMatImage;
        }

        return null;
    }

    /**
     * Removes only content which is inside of a contour form the MatImage
     * @param matImage MatImage to process
     * @param contour content to remove
     * @return processed MatImage
     */
    public static MatImage getRemovedContourContent(MatImage matImage, MatOfPoint contour)
    {
        MatImage maskedMatImage = new MatImage();
        matImage.copyTo(maskedMatImage);

        Imgproc.fillConvexPoly(maskedMatImage, contour, new Scalar(0, 0, 0));

        return maskedMatImage;
    }

    /**
     * Normalizes the perspective of the mat inside of the contour
     * @param matImage to normalize
     * @param contour area to normalize
     * @return
     */
    public static MatImage getNormalizedPerspective(MatImage matImage, MatOfPoint contour)
    {
        MatOfPoint boundingPoints = ContourUtilities.getBoundingPoints(contour);
        Point[] boundingPointsArray = boundingPoints.toArray();
        //Rect boundingRect = Imgproc.boundingRect(boundingPoints);

        MatImage originMat = new MatImage(4, 1, CvType.CV_32FC2);
        MatImage destinationMat = new MatImage(4, 1, CvType.CV_32FC2);

        originMat.put(0, 0, boundingPointsArray[0].x, boundingPointsArray[0].y, boundingPointsArray[1].x, boundingPointsArray[1].y,
                boundingPointsArray[3].x , boundingPointsArray[3].y, boundingPointsArray[2].x, boundingPointsArray[2].y);
        //destinationMat.put(0, 0, 0.0, 0.0, boundingRect.width, 0.0, 0.0, boundingRect.height, boundingRect.width, boundingRect.height);
        destinationMat.put(0, 0, 0.0, 0.0, 480, 0.0, 0.0, 400, 480, 400);

        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(originMat, destinationMat);

        MatImage wrappedPerspectiveMat = new MatImage();
        //Imgproc.warpPerspective(matImage, wrappedPerspectiveMat, perspectiveTransform, new Size(boundingRect.width, boundingRect.height));
        Imgproc.warpPerspective(matImage, wrappedPerspectiveMat, perspectiveTransform, new Size(480, 400));

        return wrappedPerspectiveMat;
    }

    /**
     * Warps the perspective of the mat
     * @param matImage to warp
     * @param contour area to warp into
     * @param size size of the output mat
     * @return
     */
    public static MatImage getWrappedPerspective(MatImage matImage, MatOfPoint contour, Size size)
    {
        MatOfPoint boundingPoints = ContourUtilities.getBoundingPoints(contour);
        Point[] boundingPointsArray = boundingPoints.toArray();

        MatImage originMat = new MatImage(4, 1, CvType.CV_32FC2);
        MatImage destinationMat = new MatImage(4, 1, CvType.CV_32FC2);

        originMat.put(0, 0, 0.0, 0.0, matImage.width(), 0.0, 0.0, matImage.height(), matImage.width(), matImage.height());
        destinationMat.put(0, 0, boundingPointsArray[0].x, boundingPointsArray[0].y, boundingPointsArray[1].x, boundingPointsArray[1].y,
                boundingPointsArray[3].x, boundingPointsArray[3].y, boundingPointsArray[2].x, boundingPointsArray[2].y);

        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(originMat, destinationMat);

        MatImage wrappedPerspectiveMat = new MatImage();
        Imgproc.warpPerspective(matImage, wrappedPerspectiveMat, perspectiveTransform, size);

        return wrappedPerspectiveMat;
    }

    /**
     * Applies basic morphological operations like dilate and erode to a MatImage
     * @param matImage MatImage to apply operations
     * @param erodeAmount how much to erode
     * @param dilateAmount how much to dilate
     * @return MatImage with applied operations
     */
    public static MatImage getAppliedBasicMorphOperations(MatImage matImage, int erodeAmount, int dilateAmount)
    {
        MatImage basicMorphMat = new MatImage();
        matImage.copyTo(basicMorphMat);

        if(erodeAmount > 0)
        {
            Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(erodeAmount, erodeAmount));

            Imgproc.erode(basicMorphMat, basicMorphMat, erodeElement);
            Imgproc.erode(basicMorphMat, basicMorphMat, erodeElement);
        }
        if(dilateAmount > 0)
        {
            Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(dilateAmount, dilateAmount));

            Imgproc.dilate(basicMorphMat, basicMorphMat, dilateElement);
            Imgproc.dilate(basicMorphMat, basicMorphMat, dilateElement);
        }

        return basicMorphMat;
    }

    /**
     * Overlays constructed image on video stream
     * @param capturedMat mat to blend with
     * @param toOverlay constructed map image
     * @param contour contour to overlay on
     */
    public static MatImage getBlendedMatImage(MatImage capturedMat, MatImage toOverlay, MatOfPoint contour)
    {
        MatImage wrappedMapMat = MatImageProcessing.getWrappedPerspective(toOverlay, contour, capturedMat.size());
        MatImage removedContourMat = MatImageProcessing.getRemovedContourContent(capturedMat, contour);
        MatImage blendedMat = new MatImage();
        org.opencv.core.Core.bitwise_or(removedContourMat, wrappedMapMat, blendedMat);

        return blendedMat;
    }

    /**
     * Constructs an image to overlay on video stream from calculated matrix
     * @param matrix filled matrix to construct from
     * @param buildingImages images used for constructing output image
     * @return constructed image
     */
    public static MatImage constructMatImageFromMatrix(Matrix<Integer> matrix, ArrayList<BufferedImage> buildingImages)
    {
        ArrayList<BufferedImage> buffImages = new ArrayList<>();

        for (int y = 0; y < matrix.getRows(); y++)
        {
            for (int x = 0; x < matrix.getCols(); x++)
            {
                int value = matrix.get(x, y);

                buffImages.add(buildingImages.get(value));
            }
        }

        if(buffImages.size() > 0)
        {
            return new MatImage(buffImages, matrix.getCols(), matrix.getRows());
        }
        else
        {
            return null;
        }
    }

}
