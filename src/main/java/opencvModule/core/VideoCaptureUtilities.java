package opencvModule.core;

import org.opencv.videoio.VideoCapture;

import java.util.concurrent.TimeUnit;

/**
 * Created by Saulius Stankevičius.
 */
public class VideoCaptureUtilities
{
    private VideoCaptureUtilities()
    {

    }

    /**
     * Starts video capture
     * @return VideoCapture if successful NULL if not
     */
    public static VideoCapture startCapture()
    {
        VideoCapture videoCapture = new VideoCapture();
        if(!videoCapture.isOpened())
        {
            try
            {
                videoCapture.open(0);
                TimeUnit.MILLISECONDS.sleep(50);
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        if(!videoCapture.isOpened())
        {
            return null;
        }
        else
        {
            return videoCapture;
        }
    }

    /**
     * Stops video capture
     * @return 0 if successful 1 if not
     */
    public static int stopCapture(VideoCapture videoCapture)
    {
        if(videoCapture != null && videoCapture.isOpened())
        {
            try
            {
                videoCapture.release();
                TimeUnit.MILLISECONDS.sleep(10);
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        if((videoCapture != null && videoCapture.isOpened()) || videoCapture == null)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
