package opencvModule.imageOut;

import java.awt.*;

/**
 * Created by Saulius Stankevičius.
 */
public interface ImagePanelInterface
{
    /**
     * Draws an image on ImagePanel
     * @param image image to display on panel
     */
    void refreshImage(Image image);
}
