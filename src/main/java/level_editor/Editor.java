package level_editor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import database.utility.*;
import javax.swing.JFrame;
import java.util.Vector;
/**
 * Created by bruar on 5/15/2016.
 */
public class Editor extends JFrame implements KeyListener, MouseListener {

    private MapCanvas canvas = new MapCanvas(this);
    private FileToolBar fileBar = new FileToolBar(48,1,this);
    private TileNavigator tileNavigator = new TileNavigator(this);
    private TileViewer tileViewer = new TileViewer(this);

    public Editor(MapData data){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setSize(850,700);
        setFocusable(true);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("BomberMan Level Editor");
        setVisible(true);
        addKeyListener(this);
        addMouseListener(this);

        fileBar.setBounds(0,0,600,50);
        canvas.setBounds(0,50,600,900);
        tileNavigator.setBounds(600,0,900,700);
        add(fileBar);
        add(canvas);
        add(tileNavigator);

        int w = data.getMapWidth();
        int h = data.getMapLength();
        String name = data.getMapName();
        String tiles = data.getMapData();

        Vector<Integer> tls = new Vector<Integer>();
        for(int i = 0; i < tiles.length(); i++){
            tls.add(tiles.charAt(i)- '0');
        }
        canvas.openFile(w,h,tls,name);
    }
    public Editor(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setSize(850,700);
        setFocusable(true);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("BomberMan Level Editor");
        setVisible(true);
        addKeyListener(this);
        addMouseListener(this);
        fileBar.setBounds(0,0,600,50);
        canvas.setBounds(0,50,600,900);
        tileNavigator.setBounds(600,0,900,700);
        add(fileBar);
        add(canvas);
        add(tileNavigator);
        canvas.openNew();

    }

    public void updateTileViewer(Tile tile, String mode) {
        tileViewer.setDrawTile(tile);
        tileViewer.setMode(mode);
    }

    public void setCanvas(MapCanvas cnv){this.canvas = cnv;}
    public MapCanvas getCanvas(){return canvas;}

    public void setFileBar(FileToolBar ftb) {this.fileBar = ftb;}
    public FileToolBar getFileBar(){return fileBar;}

    public void setTileNavigator(TileNavigator tn){this.tileNavigator = tn;}
    public TileNavigator getTileNavigator(){return tileNavigator;}

    public void setTileViewer(TileViewer tv){this.tileViewer = tv;}
    public TileViewer getTileViewer(){return tileViewer;}
    @Override
    public void keyPressed(KeyEvent e){
        // TODO auto-generated method
    }
    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }
}




