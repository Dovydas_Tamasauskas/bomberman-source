package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MapCanvas extends JPanel implements MouseListener {

    private String mode = "small";
    private EditorMap editorMap = null;
    private boolean xOverflow;
    private boolean yOverflow;
    private Editor editor;

    public MapCanvas(Editor ed) {
        this.setBackground(Color.gray);
        this.setSize(550, 600);
        this.setDoubleBuffered(true);
        this.addMouseListener(this);

        this.editor = ed;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        if (editorMap != null) {
            editorMap.draw(g2d);
        }
    }

    public void openNew() {
        int newx = Integer.parseInt(JOptionPane.showInputDialog("Horizontal tiles ?(5-20)"));
        int newy = Integer.parseInt(JOptionPane.showInputDialog("Vertical tiles ?(5-20)"));
        String name = JOptionPane.showInputDialog("Map name");
        if (newx * 50 > 600) {
            xOverflow = true;
        }
        if (newy * 50 > 550) {
            yOverflow = true;
        }
        editorMap = new EditorMap(newx, newy, name);
        repaint();
    }
    public void openFile(int w, int h, Vector<Integer> tiles,String nm) {
        editorMap = new EditorMap(w,h,tiles,nm);
        repaint();
    }
    public void addRow(){
        editorMap.addRowTile();
        repaint();
    }
    public void addCol(){
        editorMap.addColumnTile();
        repaint();
    }
    public void remRow(){
        editorMap.removeRowTile();
        repaint();
    }
    public void remCol(){
        editorMap.removeColTile();
        repaint();
    }
    public void setEditorMap(EditorMap ed){this.editorMap = ed;}
    public EditorMap getEditorMap() {return editorMap;}

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getButton() == MouseEvent.BUTTON1) {
            if (mode.equals("small")) {
                if (e.getX() < editorMap.getWidth() && e.getY() < editorMap.getHeight()) {
                    editorMap.addTile(e.getX() / 30, e.getY() / 30, editor.getTileViewer().getDrawTile());
                }
            }
            repaint();
        }

    }
    private void setMode(String s){this.mode = s;}
    private String getMode() {return mode;}

    public void SetEditor(Editor ed){this.editor = ed;}
    public Editor getEditor(){return editor;}


    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }
}
