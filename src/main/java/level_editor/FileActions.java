package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */

import bomberman.Game;
import database.utility.DatabaseTable;
import database.utility.MapData;
import database.utility.QueryExecutor;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import static com.sun.java.accessibility.util.AWTEventMonitor.addWindowListener;

public class FileActions {
    public static void perform(String action, Editor ed) throws IOException {
        if (action.equals("saveFile")) {
            saveFile(ed);
        }
        if (action.equals("openFile")) {
            openFile(ed);
        }
        if (action.equals("newFile")) {
            newFile();
            ed.getCanvas().openNew();
        }
        if (action.equals("addRows")){
            addRow();
            ed.getCanvas().addRow();
        }
        if(action.equals("addCol")) {
            addCol();
            ed.getCanvas().addCol();
        }
        if(action.equals("remRow")) {
            ed.getCanvas().remRow();
        }
        if(action.equals("remCol")){
            ed.getCanvas().remCol();
        }

    }
    public static void saveFile(Editor editor) throws IOException {
        String mapname = editor.getCanvas().getEditorMap().getName();
        BufferedWriter ras = new BufferedWriter(new FileWriter("maps/" + mapname + ".map"));
        ras.write(String.valueOf(editor.getCanvas().getEditorMap().getWidth() / 30) + " "
                + String.valueOf(editor.getCanvas().getEditorMap().getHeight() / 30) + " ");
        Vector<Tile> currentListas = editor.getCanvas().getEditorMap().getTileListL1();
        for (int i = 0; i < currentListas.size(); i++) {
            ras.write(currentListas.get(i).getR());
        }
        ras.close();
        System.out.println("file saved");
    }
    public static void openFile(Editor editor) throws IOException {
        String map = "";
        Vector<Tile> currentListas = editor.getCanvas().getEditorMap().getTileListL1();
        for (int i = 0; i < currentListas.size(); i++)
            map += Integer.toString(currentListas.get(i).getR()-48);

        MapData data = new MapData(QueryExecutor.getUniqueIdentification(DatabaseTable.MAPS_TABLE),
                                   editor.getCanvas().getEditorMap().getWidth()/30,
                                    editor.getCanvas().getEditorMap().getHeight() / 30,
                                    map,
                editor.getCanvas().getEditorMap().getName()
                );
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we)
            {
                String ObjButtons[] = {"Yes","No"};
                int PromptResult = JOptionPane.showOptionDialog(null,"Do you want to save map in database?","Online Examination System",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
                if(PromptResult==JOptionPane.YES_OPTION)
                {
                    QueryExecutor.addMapDataToTable(data);

                }
            }
        });
        System.out.println(data.getMapID()+data.getMapName()+data.getMapLength()+data.getMapWidth()+data.getMapData());
        new Game(data);
        System.exit(0);
    }

    public static void newFile() {System.out.println("new file");
    }

    public static void addRow() { System.out.println("add rows");}
    public static void addCol() { System.out.println("add cols");}
}
