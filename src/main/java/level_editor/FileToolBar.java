package level_editor;

/**
 * Created by bruar on 5/16/2016.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JPanel;

public class FileToolBar extends JPanel implements MouseListener {
    private Button newButton, saveButton, openButton, addButton, addCol, remRow, remCol;
    private Vector<Button> buttonList = new Vector<Button>();
    private int buttonSize;
    private int padding;

    Editor editor;

    public FileToolBar(int bSize, int padd, Editor ed) {
        this.setBackground(Color.gray);
        this.setSize(300, 50);
        this.setDoubleBuffered(true);
        setFocusable(true);

        editor = ed;

        buttonSize = bSize;
        padding = padd;

        buttonList.add(newButton = new Button(bSize, bSize, "new.png", "newFile"));
        buttonList.add(saveButton = new Button(bSize, bSize, "save.png", "saveFile"));
        buttonList.add(openButton = new Button(bSize, bSize, "open.png", "openFile"));
        buttonList.add(addButton = new Button(bSize, bSize, "plusrows.png", "addRows"));
        buttonList.add(remRow = new Button(bSize, bSize, "minusrows.png", "remRow"));
        buttonList.add(addCol = new Button(bSize, bSize, "pluscols.png", "addCol"));
        buttonList.add(remCol = new Button(bSize, bSize, "minuscols.png", "remCol"));

        addMouseListener(this);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < buttonList.size(); i++) {
            buttonList.get(i).draw((i * 50) + padding, 0 + padding, g);
        }
    }

    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getButton() == MouseEvent.BUTTON1) {
            for (int i = 0; i < buttonList.size(); i++) {
                if (Mouse.isInRegion(e.getX(), e.getY(), buttonList.get(i).getPosX(), buttonList.get(i).getPosY(),
                        buttonList.get(i).getPosX() + buttonList.get(i).getWidth(),
                        buttonList.get(i).getPosY() + buttonList.get(i).getHeight())) {
                    try {
                        FileActions.perform(buttonList.get(i).getAction(), editor);
                    } catch (FileNotFoundException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }
        }
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }
}
