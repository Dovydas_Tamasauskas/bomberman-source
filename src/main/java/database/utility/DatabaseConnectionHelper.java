package database.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * <code>DatabaseConnectionHelper</code> contains utility methods for obtaining
 * a valid connection to the server. This class cannot be instantiated.
 *
 * @author Sarunas Sarakojis
 */
public class DatabaseConnectionHelper {

    private static final String CONNECTION_URL = "jdbc:mysql://stud.if.ktu.lt/";
    private static final String DATABASE = "sarsar2";
    private static final transient String USER = "sarsar2";
    private static final transient String PASSWORD;

    static {
        PASSWORD = Character.toString((char) 0x73) + Character.toString((char) 0x61) +
                Character.toString((char) 0x72) + Character.toString((char) 0x75) +
                Character.toString((char) 0x6E) + Character.toString((char) 0x61) +
                Character.toString((char) 0x73) + Character.toString((char) 0x39);
    }

    private DatabaseConnectionHelper() {
        /*
         * This class cannot be instantiated
         * since it contains static methods only.
         */
    }

    /**
     * Method returns a new connection to the <code>stud.if.ktu.lt</code> server.
     * Once the {@link Connection} has been returned, all statements can be performed
     * on the database, by creating {@link java.sql.Statement}s and executing them.
     * The returned connection <em>MUST</em> be closed.
     *
     * @return an open {@link Connection} to the <code>stud.if.ktu.lt</code> server or
     * {@code null} if method did not receive connection.
     */
    public static Connection getConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(CONNECTION_URL + DATABASE, USER, PASSWORD);
        } catch (SQLException ignored) {
            // TODO output error messages using Logger
        }

        return connection;
    }

    /**
     * Method tries to close a given {@link Connection}. If the {@link Connection#close()}
     * throws an <em>exception</em> then it gets handled by a <em>Logger</em>.
     *
     * @param connection connection to close
     */
    public static void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            // TODO output error messages using Logger
        }
    }
}
