package database.utility;

/**
 *
 * @author Sarunas Sarakojis
 */
public enum DatabaseTable {
    MAPS_TABLE("GameMaps");

    private String name;

    /**
     *
     */
    public enum MapsTable {
        ID("identification"),
        WIDTH("width"),
        LENGTH("length"),
        DATA("blocksData"),
        NAME("name");

        private String name;

        MapsTable(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    DatabaseTable(String name) {
        this.name = name;
    }

    /**
     * Method returns a name for <code>this</code> enumerator.
     *
     * @return a name for the current enumerator
     */
    @Override
    public String toString() {
        return name;
    }

}
