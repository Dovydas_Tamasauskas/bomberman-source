package database;

import database.utility.DatabaseTable;
import database.utility.MapData;
import database.utility.QueryExecutor;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test cases for {@link QueryExecutor}.
 *
 * @author Sarunas Sarakojis
 */
public class QueryExecutorTest {

    @Test
    public void testRowsSelectionFromGameMapTable() throws SQLException {
        List<MapData> mapData = QueryExecutor.selectMapDataFromDatabase();
        int rowsCount = QueryExecutor.getTableRowsCount(DatabaseTable.MAPS_TABLE);

        assertNotNull("Result should not be null", mapData);
        assertTrue("Amount of parsed maps should be equal to rows count", mapData.size() == rowsCount);
    }

    @Test
    public void testMapDataInsertionToDatabase() {
        MapData mapData = new MapData(QueryExecutor.getUniqueIdentification(DatabaseTable.MAPS_TABLE), 13, 13,
                "testMapDataX", "testX");
        int countBeforeInsert = QueryExecutor.getTableRowsCount(DatabaseTable.MAPS_TABLE);

        QueryExecutor.addMapDataToTable(mapData);

        assertTrue("One row was added", QueryExecutor
                .getTableRowsCount(DatabaseTable.MAPS_TABLE) - countBeforeInsert == 1);
        QueryExecutor.deleteMapDataFromTable(mapData);
        assertTrue("One row should be deleted", QueryExecutor
                .getTableRowsCount(DatabaseTable.MAPS_TABLE) == countBeforeInsert);
    }

    @Test
    public void testMultipleMapDataInsertionToDatabase() {
        int uniqueID = QueryExecutor.getUniqueIdentification(DatabaseTable.MAPS_TABLE);
        MapData mapData1 = new MapData(uniqueID, 11, 11, "testMapData1", "test1");
        MapData mapData2 = new MapData(uniqueID + 1, 12, 12, "testMapData2", "test2");
        List<MapData> mapData = getListOfMapData(mapData1, mapData2);
        int countBeforeInsert = QueryExecutor.getTableRowsCount(DatabaseTable.MAPS_TABLE);

        QueryExecutor.addMapDataToTable(mapData);

        assertTrue("Two rows were added", QueryExecutor
                .getTableRowsCount(DatabaseTable.MAPS_TABLE) - countBeforeInsert == 2);
        QueryExecutor.deleteMapDataFromTable(mapData);
        assertTrue("Two rows should be deleted", QueryExecutor
                .getTableRowsCount(DatabaseTable.MAPS_TABLE) == countBeforeInsert);
    }

    private List<MapData> getListOfMapData(MapData... mapData) {
        List<MapData> data = new ArrayList<>(mapData.length);

        Collections.addAll(data, mapData);

        return data;
    }
}
