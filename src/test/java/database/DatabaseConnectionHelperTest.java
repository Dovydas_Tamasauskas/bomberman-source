package database;

import database.utility.DatabaseConnectionHelper;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Test cases for the {@link DatabaseConnectionHelper}.
 *
 * @author Sarunas Sarakojis
 */
public class DatabaseConnectionHelperTest {

    @Test
    public void testConnectionToDatabase() throws SQLException {
        Connection connection = DatabaseConnectionHelper.getConnection();

        assertNotNull("Connection should be successful", connection);
        assertFalse("Connection should be in r/w mode", connection.isReadOnly());
    }

    @Test
    public void testCloseConnection() {
        Connection connection = DatabaseConnectionHelper.getConnection();

        assert connection != null;
        DatabaseConnectionHelper.closeConnection(connection);
    }
}
