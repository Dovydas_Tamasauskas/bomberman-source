package opencvTest.coreTest;

import opencvModule.core.ContourUtilities;
import org.junit.Test;
import org.opencv.core.*;
import org.opencv.imgproc.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Saulius Stankevičius.
 * Class for testing ContourUtilities methods
 */
public class ContourUtilitiesTest
{
    @Test
    public void testBoundingPoints1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 2), new Point(0, 2), new Point(2, 0));
        Rect testSampleRect = Imgproc.boundingRect(testSample);

        assertEquals((int)testSampleRect.area(), (int)Imgproc.boundingRect(ContourUtilities.getBoundingPoints(testSample)).area());
    }

    @Test
    public void testBoundingPoints2()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(1, 1), new Point(2, 2), new Point(0, 2), new Point(2, 0));
        Rect testSampleRect = Imgproc.boundingRect(testSample);

        assertEquals((int)testSampleRect.area(), (int)Imgproc.boundingRect(ContourUtilities.getBoundingPoints(testSample)).area());
    }

    @Test
    public void testBoundingPoints3()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(1, 1), new Point(2, 2), new Point(0, 2), new Point(2, 0));

        assertEquals(4, ContourUtilities.getBoundingPoints(testSample).toArray().length);
    }

    @Test
    public void testIfAreaBiggerThan1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));

        assertEquals(true, ContourUtilities.checkIfContourAreaLargerThan(testSample, 3));
    }

    @Test
    public void testIfAreaBiggerThan2()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));

        assertEquals(false, ContourUtilities.checkIfContourAreaLargerThan(testSample, 4));
    }

    @Test
    public void testIfAreaBiggerThan3()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));

        assertEquals(false, ContourUtilities.checkIfContourAreaLargerThan(testSample, 5));
    }

    @Test
    public void testIfAreaBiggerThan4()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        assertEquals(false, ContourUtilities.checkIfContourAreaLargerThan(null, 4));
    }

    @Test
    public void testMedianContourArea1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample1 = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));
        MatOfPoint testSample2 = new MatOfPoint(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));
        MatOfPoint testSample3 = new MatOfPoint(new Point(0, 0), new Point(3, 0), new Point(3, 3), new Point(0, 3));

        ArrayList<MatOfPoint> contours = new ArrayList<>();
        contours.add(testSample1);
        contours.add(testSample2);
        contours.add(testSample3);

        assertEquals(4, ContourUtilities.getMedianContourArea(contours).intValue());
    }

    @Test
    public void testMedianContourArea2()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample1 = new MatOfPoint(new Point(1, 1), new Point(3, 1), new Point(3, 3), new Point(1, 3));
        MatOfPoint testSample2 = new MatOfPoint(new Point(1, 1), new Point(2, 1), new Point(2, 2), new Point(1, 2));
        MatOfPoint testSample3 = new MatOfPoint(new Point(1, 1), new Point(4, 1), new Point(4, 4), new Point(1, 4));

        ArrayList<MatOfPoint> contours = new ArrayList<>();
        contours.add(testSample3);
        contours.add(testSample1);
        contours.add(testSample2);

        assertEquals(4, ContourUtilities.getMedianContourArea(contours).intValue());
    }

    @Test
    public void testMedianContourArea3()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample1 = new MatOfPoint(new Point(1, 1), new Point(3, 1), new Point(3, 3), new Point(1, 3));
        MatOfPoint testSample2 = new MatOfPoint(new Point(1, 1), new Point(2, 1), new Point(2, 2), new Point(1, 2));
        MatOfPoint testSample3 = new MatOfPoint(new Point(1, 1), new Point(4, 1), new Point(4, 4), new Point(1, 4));
        MatOfPoint testSample4 = new MatOfPoint(new Point(1, 1), new Point(5, 1), new Point(5, 5), new Point(1, 5));

        ArrayList<MatOfPoint> contours = new ArrayList<>();
        contours.add(testSample1);
        contours.add(testSample2);
        contours.add(testSample3);
        contours.add(testSample4);

        assertEquals(6, ContourUtilities.getMedianContourArea(contours).intValue());
    }

    @Test
    public void testMedianContourArea4()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample1 = new MatOfPoint(new Point(2, 2), new Point(4, 2), new Point(4, 4), new Point(2, 4));
        MatOfPoint testSample2 = new MatOfPoint(new Point(2, 2), new Point(3, 2), new Point(3, 3), new Point(2, 3));
        MatOfPoint testSample3 = new MatOfPoint(new Point(2, 2), new Point(5, 2), new Point(5, 5), new Point(2, 5));
        MatOfPoint testSample4 = new MatOfPoint(new Point(2, 2), new Point(6, 2), new Point(6, 6), new Point(2, 6));

        ArrayList<MatOfPoint> contours = new ArrayList<>();
        contours.add(testSample3);
        contours.add(testSample1);
        contours.add(testSample4);
        contours.add(testSample2);

        assertEquals(6, ContourUtilities.getMedianContourArea(contours).intValue());
    }
}
