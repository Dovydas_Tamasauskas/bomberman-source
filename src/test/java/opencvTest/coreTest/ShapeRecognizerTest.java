package opencvTest.coreTest;

import opencvModule.core.ShapeRecognizer;
import org.junit.Test;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Saulius Stankevičius.
 * Class for testing ShapeRecognizer methods
 */
public class ShapeRecognizerTest
{
    @Test
    public void testShape1()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2));

        assertEquals("TRIANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape2()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));

        assertEquals("RECTANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape3()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(4, 0), new Point(2, 2), new Point(1, 2));

        assertEquals("TRIANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape4()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(4, 0), new Point(2, 2));

        assertEquals("TRIANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape5()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(4, 0), new Point(4, 2), new Point(5, 2), new Point(7, 7),
                new Point(10, 5), new Point(20, 20));

        assertEquals(null, ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape6()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(2, 0), new Point(2, 2));

        assertEquals("TRIANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape7()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(1, 0), new Point(4, 0), new Point(2, 10));

        assertEquals("TRIANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape8()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 2), new Point(0, 2));

        assertEquals("RECTANGLE", ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape9()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(1, 0), new Point(4, 0), new Point(2, 100));

        assertEquals(null, ShapeRecognizer.getShapeName(testSample));
    }

    @Test
    public void testShape10()
    {
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

        MatOfPoint testSample = new MatOfPoint(new Point(1, 0), new Point(4, 0), new Point(200, 10));

        assertEquals(null, ShapeRecognizer.getShapeName(testSample));
    }

}
