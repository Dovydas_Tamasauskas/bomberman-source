# Bomberman Repository README #

This README documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository has all the information of project *Bomberman*, including source code files and documents: 
requirements, various diagrams, algorithm descriptions ant etc.

### How to set up? ###

* Clone repository: git clone https://bitbucket.org/semestroprojektasif13/bomberman.git
* Once the project was cloned, start IntelliJ IDEA:
* On welcome screen of IntelliJ IDEA, press "Import Project".
* Navigate to the root of the Bomberman project and select *build.gradle* file.
* On the next screen, select *Use Default gradle wrapper*
* Wait for the project to build...
* When the project has loaded, go to *Run -> Edit Configurations...*
* Create new *Application* type configuration.
* Set the following properties:
* * *Main class:* view.MainWindow
* * *Use classpath of module:* Bomberman_main
* Then add required libraries for the project:
* * Add OpenCV libraries: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select ./lib/opencv-310.jar file.
* * Add OpenCV native library location: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select ./lib/x64/ folder. IntelliJ automatically recognizes this as a native library (no need to set VM argument).
* * Add Connector/J library: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select ./lib/mysql-connector-java-\*/mysql-connector-java-\*-bin.jar file.
* * Add TestFX library for javafx GUI testing: File -> Project Structure -> Project Settings -> Libraries -> New Project Library (+), select from Maven -> search testfx -> Core:3.1.2 SNAPSHOT 
* * Press *OK*.
* Run the project.

### Project structure ###

All production java code files are placed in *src/main/java/* directory, whereas JUnit tests are placed in *src/test/java/* directory. Resources are placed in *src/main/resources/* and *src/test/resources/* directories.

### Team ###

Team consists of the following members: Arturas Bruckus, Aivaras Dziaugys, Dovydas Tamasauskas, Saulius Stankevicius, Sarunas Sarakojis.